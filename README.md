# DF ST - The Bard

Dwarf Fortress Storyteller - The bard is a legends viewer that uses 
[DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller) as its data-source.

This project started as an example how to use DF Storyteller and to find the shortcomings
of the API.

The current state of the project still very focused on the development of the API.
So you will see small experiments with code that might be moved into DF Storyteller.

You are free to use this projects as a starting point for creating your own visualizers.
Although it might require some refactoring/restructuring.

If you are just getting started with using the API we recommend first taking a look at 
the [example sketches here](https://gitlab.com/df_storyteller/df-storyteller-sketches).

## Screenshots

![HF list](./img/HF-list.png)
![HF view1](./img/HF-view1.png)
![HF view2](./img/HF-view2.png)
![HF view3](./img/HF-view3.png)
![HF view data](./img/HF-view-data.png)

## Install

* First install [DF Storyteller](https://dfstoryteller.com/).
You can find a [guide here](https://guide.dfstoryteller.com/).
* Import a world and start the API service (`import` and `start` subcommand).
* Download this repo can copy it into the `serve-paintings` folder created by DF Storyteller. 
So the path should be `serve-paintings/the-bard/index.html`.
* Open the url: http://127.0.0.1:20350/paintings/the-bard

If you have problems join our [Discord](https://discord.gg/aAXt6uu).

## Want to contribute?

If you want to contribute to the examples feel free to open a merge request.
For more information and out Code of Conduct see the main project 
[DF Storyteller](https://gitlab.com/df_storyteller/df-storyteller).

## License

Licensed under either of [Apache License, Version 2.0](LICENSE-APACHE) or
[MIT license](LICENSE-MIT) at your option.
Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in
DF Storyteller Sketches by you, as defined in the Apache-2.0 license, shall be dual licensed as
above, without any additional terms or conditions.