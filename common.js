// ---------- Skill / level -----------

// This function might get moved into DF Storyteller in the future.
function get_level(xp){
    if( xp < 0 ){
        return 0;
    }
    for( let i = 1; i <= 15; i++){
        if( xp < xp_for_level(i) ){
            return i-1;
        }
    }
    return 15;
}

function xp_for_level(level){
    if( level == 0){
        return 0;
    }
    return (400 + 100 * level + xp_for_level(level-1));
}

let level_names = ["Dabbling","Novice","Adequate","Competent","Skilled","Proficient",
    "Talented", "Adept", "Expert", "Professional", "Accomplished", "Great", "Master", 
    "High Master", "Grand Master", "Legendary"];
let level_colors = ["#808000", "#C0C0C0", "#FFFFFF", "#008080", "#008080", "#008080",
    "#00FFFF", "#00FFFF", "#00FFFF", "#00FF00", "#00FF00", "#00FF00", "#00FF00", 
    "#00FF00", "#00FF00", "#FF00FF"];

// ---------- Historical Event to Description -----------

// VERY simple Historical Event to text convertor
// It was discussed on Discord and https://gitlab.com/df_storyteller/df-storyteller/-/issues/85
// That there should be a general library created to do this type of translation.
// Check this document for more info (if any is available): 
// https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/become_a_painter.md
// One implementation that is in progress is: 
// https://github.com/Logodaedalus/DF-Storyteller-Narrativization
function HE_to_string(he){
    // Add year
    let string = `In ${he.year}, `;
    // Look at the type and select the right string
    switch(he.type) {
        case "change_hf_state":
            switch(he.state){
                case "settled":
                    string += `${he.hf_id} settled in ${he.site_id}`;
                    break;
            };
            break;
        case "change_hf_job":
            if( he.new_job == "standard" ){
                string += `${he.hf_id} stopped being a ${he.old_job} in ${he.site_id}`;
            }else if( he.old_job == "standard" ){
                string += `${he.hf_id} became a ${he.new_job} in ${he.site_id}`;
            }else{
                string += `${he.hf_id} gave up being a ${he.old_job} to become a ${he.new_job} in ${he.site_id}`;
            }
            break;
        case "hf_died":
            let death_cause = he.death_cause.replace('_',' ');
            let slayer_race = "";
            if( he.slayer_race ){
                slayer_race = he.slayer_race.toLowerCase();
            }
            string += `${he.hf_id} was ${death_cause} by the ${slayer_race} ${he.slayer_hf_id} in ${he.subregion_id}`;
            break;
        default:
            string += `Unknown Event: ${he.type}`;
            break;
    }
    return string;
}

// ---------- Group By -----------

function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
            map.set(key, [item]);
        } else {
            collection.push(item);
        }
    });
    return map;
}

// ---------- Vertical Line plugin -----------

// Vertical line
const verticalLinePlugin = {
    getLinePosition: function (chart, line_x_pos) {
        const meta = chart.getDatasetMeta(0); // first dataset is used to discover X coordinate of a point
        const data = meta.data;
        if( data.length > line_x_pos && line_x_pos >= 0){
            return data[line_x_pos]._model.x;
        }
        return NaN;
    },
    renderVerticalLine: function (chartInstance, line) {
        const lineLeftOffset = this.getLinePosition(chartInstance, line.x);
        const scale = chartInstance.scales['y-axis-0'];
        const context = chartInstance.chart.ctx;

        // render vertical line
        context.beginPath();
        context.strokeStyle = line.color;
        context.moveTo(lineLeftOffset, scale.top);
        context.lineTo(lineLeftOffset, scale.bottom);
        context.stroke();

        // write label
        context.fillStyle = line.color;
        context.textAlign = 'left';
        // context.fillText(line.text, lineLeftOffset+3, (scale.bottom - scale.top) / 4 + scale.top);
        context.fillText(line.text, lineLeftOffset+3, scale.top + 10);
    },

    afterDatasetsDraw: function (chart, easing) {
        if (chart.config.verticalLines) {
            chart.config.verticalLines.forEach(line => this.renderVerticalLine(chart, line));
        }
    }
};
Chart.plugins.register(verticalLinePlugin);

// ---------- Group Skills -----------

function construct_skill_group_data(data){
    return [
        count_skill_group(0, data),
        count_skill_group(1, data),
        count_skill_group(2, data),
        count_skill_group(3, data),
        count_skill_group(4, data),
        count_skill_group(5, data),
        count_skill_group(6, data),
        count_skill_group(7, data),
        count_skill_group(8, data),
        count_skill_group(9, data),
        count_skill_group(10, data),
        count_skill_group(11, data),
        count_skill_group(12, data),
        count_skill_group(13, data),
        count_skill_group(14, data),
        count_skill_group(15, data),
        count_skill_group(16, data),
    ];
}

// This function is rough implementation and does NOT include every skill yet.
// This might get moved into DF Storyteller in the future.
function count_skill_group(group_nr, data){
    var skill_group = [];
    switch( group_nr ){
        case 0: // Miner
            skill_group = ["MINING"];
            break;
        case 1: // Woodworker
            skill_group = ["BOWYER","CARPENTRY","WOODCUTTING"];
            break;
        case 2: // Stoneworker
            skill_group = ["DETAILSTONE", "MASONRY"];
            break;
        case 3: // Ranger
            skill_group = ["??","ANIMALCARE","DISSECT_VERMIN","ANIMALTRAIN","TRAPPING"];
            break;
        case 4: // Doctor
            skill_group = ["SET_BONE","??","DIAGNOSE","SURGERY","SUTURE","DRESS_WOUNDS"];
            break;
        case 5: // Military
            skill_group = ["ARMOR","AXE","BITE","BLOWGUN","BOW","CROSSBOW","DAGGER","DISCIPLINE",
            "DODGING","GRASP_STRIKE","HAMMER","MACE","MELEE_COMBAT","MILITARY_TACTICS","PIKE",
            "SPEAR","STANCE_STRIKE","SWORD","THROW","WHIP","WRESTLING"];
            break;
        case 6: // Farmer
            skill_group = ["BEEKEEPING","BREWING","BUTCHER","CHEESEMAKING","COOK","HERBALISM",
            "LYE_MAKING","MILK","MILLING","PLANT","POTASH_MAKING","PRESSING",
            "PROCESSPLANTS","SHEARING","SOAP_MAKING","SPINNING","TANNER"];
            break;
        case 7: // Fishery worker
            skill_group = ["FISH","DISSECT_FISH","PROCESSFISH"];
            break;
        case 8: // Metalsmith
            skill_group = ["FORGE_ARMOR","FORGE_FURNITURE","FORGE_WEAPON","METALCRAFT"];
            break;
        case 9: // Jeweler
            skill_group = ["CUTGEM","ENCRUSTGEM"];
            break;
        case 10: // Craftsdwarf
            skill_group = ["BOOKBINDING","BONECARVE","CLOTHESMAKING","GLASSMAKER","GLAZING",
            "LEATHERWORK","PAPERMAKING","POTTERY","STONECRAFT","EXTRACT_STRAND","WAX_WORKING",
            "WEAVING","WOODCRAFT"];
            break;
        case 11: // Engineer
            skill_group = ["MECHANICS","OPERATE_PUMP","SIEGECRAFT","SIEGEOPERATE"];
            break;
        case 12: // Administrator
            skill_group = ["APPRAISAL","DESIGNBUILDING","ORGANIZATION","RECORD_KEEPING"];
            break;
        case 13: // Broker
            skill_group = ["COMEDY","CONVERSATION","FLATTERY","INTIMIDATION","JUDGING_INTENT",
            "LYING","NEGOTIATION","PERSUASION"];
            break;
        case 14: // Miscellaneous
            skill_group = ["CLIMBING","CONSOLE","LEADERSHIP","READING","TEACHING","TRACKING"];
            break;
        case 15: // Performance
            skill_group = ["DANCE","SING","MAKE_MUSIC","POETRY","SPEAKING",
            "PLAY_KEYBOARD_INSTRUMENT","PLAY_PERCUSSION_INSTRUMENT"
            ,"PLAY_STRINGED_INSTRUMENT","PLAY_WIND_INSTRUMENT"];
            break;
        case 16: // Scholar
            skill_group = ["CRITICAL_THINKING","LOGIC","MATHEMATICS","ASTRONOMY",
            "CHEMISTRY","GEOGRAPHY","OPTICS_ENGINEER","FLUID_ENGINEER","WRITING",
            "KNOWLEDGE_ACQUISITION"];
            break;
    }
    let score = 0;
    for (let i in data.skills){
        let skill = data.skills[i];
        if( skill_group.includes(skill.skill) ){
            let level = get_level(skill.total_ip);
            score += level;
        }
    }
    return score;
}